<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html>
<head>
    <title>Users page</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div>
                <a href="/">
                    <button class="btn btn-default" type="button">Main page</button>
                </a>
                <a href="/logout">
                    <button class="btn btn-default" type="button">Main page</button>
                </a>
            </div>
            <table id="conversation" class="table table-striped">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Username</td>
                    <td>Cat pic</td>
                    <td>Banned?</td>
                </tr>
                </thead>
                <tbody>
                <#if users??>
                    <#list users as user>
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.username}</td>
                        <td><img src="${user.catUrl}" alt="" width="200" height="200"></td>
                        <td>${user.banned?c}</td>
                        <td>
                            <#if !user.banned>
                                <a href="/ban/${user.id}">Ban</a>
                                <#else>
                                <a href="/revoke_ban/${user.id}">Revoke ban</a>

                            </#if>
                        </td>

                    </tr>
                    </#list>
                <#else>
                <tr>
                    <td>No users yet...</td>
                </tr

                </#if>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>