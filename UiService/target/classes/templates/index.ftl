<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html>
<head>
    <title>Registration page</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
    <script src="js/app.js"></script>
</head>
<body>
<noscript><h2 style="color: #ff0000">Seems your browser doesn't support Javascript! Websocket relies on Javascript being
    enabled. Please enable
    Javascript and reload this page!</h2></noscript>
<div id="main-content" class="container">
    <div class="row">
        <div class="col-md-6">
            <a href="/login">
                <button class="btn btn-default" type="button">Login page</button>
            </a>

            <form class="form-inline" method="post" action="/register">
                <table>
                    <tr class="form-group">
                        <td><label for="email">Enter your username</label></td>
                        <td><input type="text" id="username" class="form-control" placeholder="Your username here..."
                                   required name="username"></td>
                    </tr>
                    <tr class="form-group">
                        <td><label for="password">Enter your password</label></td>
                        <td><input type="password" id="password" class="form-control"
                                   placeholder="Your password here..." required name="password"></td>
                    </tr>
                </table>
                <input type="hidden" id="hidden" value="" name="catUrl">
                <button id="getCat" class="btn btn-default" type="button">Get cat!</button>
                <button type="submit" class="btn btn-default">Submit!</button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <img id="catimg" src="" alt="" style="display: none; margin-top: 10px;">
        </div>
    </div>
</div>
</body>
<style>
    img {
        max-width: 500px;
        max-height: 500px;
    }
</style>
</html>