<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html>
<head>
    <title>Login page</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div id="main-content" class="container">
    <div class="row">
        <div class="col-md-6">
            <a href="/">
                <button class="btn btn-default" type="button">Registration page</button>
            </a>
            <form class="form-inline" method="post" action="/login">
                <table>
                    <tr class="form-group">
                        <td><label for="email">Enter your username</label></td>
                        <td><input type="text" id="username" class="form-control" placeholder="Your username here..."
                                   required name="username"></td>
                    </tr>
                    <tr class="form-group">
                        <td><label for="password">Enter your password</label></td>
                        <td><input type="password" id="password" class="form-control"
                                   placeholder="Your password here..." required name="password"></td>
                    </tr>
                </table>
                <button type="submit" class="btn btn-default">Submit!</button>
                <#if error??>
                    <div>${error}</div>
                </#if>
            </form>
        </div>
    </div>
</div>
</body>