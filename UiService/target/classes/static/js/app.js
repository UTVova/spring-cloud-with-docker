var stompClient = null;

function showCat(catUrl) {
    const img = $("#catimg");
    img.attr('src', catUrl);
    img.show();
}

function connect() {
    var socket = new SockJS('/messages');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/user/queue/cat_resp', function (message) {
            showCat(message.body);
            $("#hidden").val(message.body)
        });
    });
}

// function disconnect() {
//     if (stompClient !== null) {
//         stompClient.disconnect();
//     }
//     setConnected(false);
//     console.log("Disconnected");
// }

function sendCatRequest() {
    stompClient.send("/app/cat_req", {}, JSON.stringify({"email": $("#name").val()}));
}

$(function () {
    $("form").on('submit', function (e) {
        if ($("#hidden").val() === "") {
            e.preventDefault();
            alert("You must choose the cat!");
        }
        // e.preventDefault();
    });
    $(document).ready(connect);
    // $("#disconnect").click(function () {
    //     disconnect();
    // });
    $("#getCat").click(function () {
        $(this).text('Update');
        sendCatRequest();
    });
});