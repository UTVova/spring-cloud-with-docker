package ru.itis.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
@EnableWebSocket
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    public static final String ENDPOINT_CONNECT = "/messages";
    public static final String SUBSCRIBE_USER_PREFIX = "/user";
    public static final String SUBSCRIBE_CAT_RESPONSE = "/cat_resp";
    public static final String SUBSCRIBE_QUEUE = "/queue";

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker(SUBSCRIBE_QUEUE,SUBSCRIBE_CAT_RESPONSE);
        config.setApplicationDestinationPrefixes("/app");
        config.setUserDestinationPrefix(SUBSCRIBE_USER_PREFIX);
    }

    public void registerStompEndpoints(StompEndpointRegistry stompEndpointRegistry) {
        stompEndpointRegistry
                .addEndpoint(ENDPOINT_CONNECT)
                .setHandshakeHandler(new AssignPrincipalHandshakeHandler())
                .setAllowedOrigins("*")
                .withSockJS();
    }

}
