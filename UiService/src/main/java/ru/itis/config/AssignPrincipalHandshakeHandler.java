package ru.itis.config;

import org.springframework.http.server.ServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import java.security.Principal;
import java.util.Map;
import java.util.UUID;

public class AssignPrincipalHandshakeHandler extends DefaultHandshakeHandler {
    private static final String ATTR_PRINCIPAL = "__principal__";

    @Override
    protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler,
                                      Map<String, Object> attributes) {
        final String uniqueString;
        if (!attributes.containsKey(ATTR_PRINCIPAL)) {
            uniqueString = UUID.randomUUID().toString();
            attributes.put(ATTR_PRINCIPAL, uniqueString);
        }
        else {
            uniqueString = (String)attributes.get(ATTR_PRINCIPAL);
        }
        return () -> uniqueString;
    }
}