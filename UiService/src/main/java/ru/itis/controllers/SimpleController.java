package ru.itis.controllers;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.itis.amqp.RegistrationRequestsProducer;
import ru.itis.dto.LoginRequestDto;
import ru.itis.dto.RegistrationRequestDto;
import ru.itis.dto.User;
import ru.itis.dto.UserDto;
import ru.itis.exceptions.ForbiddenException;
import ru.itis.redis.RedisUserService;
import ru.itis.services.AuthService;
import ru.itis.services.JwtsHelper;
import ru.itis.services.UserService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Controller
public class SimpleController {

    @Autowired
    RegistrationRequestsProducer registrationRequestsProducer;

    @Autowired
    UserService userService;

    @Autowired
    AuthService authService;

    @Autowired
    RedisUserService redisUserService;

    @Autowired
    JwtsHelper helper;


    @GetMapping("/")
    public String index(@CookieValue(required = false) String token) {
        if (token != null && !helper.isExpired(token))
            return "redirect:/users";
        return "index";
    }

    @PostMapping("/register")
    public String registrationRequest(RegistrationRequestDto registrationRequestDto) {
        registrationRequestsProducer.send(registrationRequestDto);
        return "redirect:/";
    }

    @GetMapping("/login")
    public String login(@CookieValue(required = false) String token) {
        if (token != null && !helper.isExpired(token))
            return "redirect:/users";
        return "login";
    }

    @PostMapping("/login")
    public String loginRequest(LoginRequestDto loginRequestDto,
                               HttpServletResponse resp, RedirectAttributes redirectAttrs) {
        String token = authService.loginAndObtainToken(loginRequestDto);
        if (token != null) {
            resp.addCookie(new Cookie("token", token));
            return "redirect:/users";
        } else {
            redirectAttrs.addFlashAttribute("error", "You are not logged in");
            return "redirect:/login";
        }
    }

    @GetMapping("/logout")
    public String logout(@CookieValue String token, HttpServletResponse resp) {
        authService.logout(token);

        Cookie tokenCookie = new Cookie("token", token);
        tokenCookie.setMaxAge(0);
        resp.addCookie(tokenCookie);

        return "redirect:/login";
    }

    @GetMapping("/ban/{id}")
    public String ban(@PathVariable String id, @CookieValue String token) {
        userService.ban(token, id);
        return "redirect:/users";
    }

    @GetMapping("/revoke_ban/{id}")
    public String revokeBan(@PathVariable String id, @CookieValue String token) {
        userService.revokeBan(token, id);
        return "redirect:/users";
    }


    @GetMapping("/users")
    public String getUsers(@CookieValue String token, Model model) {
        User[] users = userService.getUsers(token);
        model.addAttribute("users", users);
        return "users_page";
    }

}
