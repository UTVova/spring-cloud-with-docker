package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.amqp.CatsProducer;
import ru.itis.dto.CatRequestDto;

import java.security.Principal;

@org.springframework.web.bind.annotation.RestController
public class RestController {
    public static final String REQUEST_CAT = "/cat_req";

    @Autowired
    CatsProducer catsProducer;

    @MessageMapping(REQUEST_CAT)
    public void requestCat(Principal principal, CatRequestDto requestMessage) {
        System.out.println(principal.getName());
        catsProducer.send(CatRequestDto.builder().uuid(principal.getName()).build());
    }
}
