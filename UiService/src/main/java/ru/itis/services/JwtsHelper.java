package ru.itis.services;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.itis.exceptions.BadRequestException;
import ru.itis.exceptions.TokenExpiredException;

import java.util.Date;

@Component
public class JwtsHelper {

    @Value("${jwt.secret:secret}")
    private String jwtSecret;

    private Claims getBody(String token) {
        Claims body;
        try {
            body = Jwts.parser()
                    .setSigningKey(jwtSecret)
                    .parseClaimsJws(token)
                    .getBody();
            return body;
        } catch (MalformedJwtException | SignatureException e) {
            e.printStackTrace();
            throw new BadRequestException();
        } catch (ExpiredJwtException e) {
            e.printStackTrace();
            throw new TokenExpiredException();
        }
    }

    public Date getExpiration(String token) {
        return getBody(token).getExpiration();
    }

    public Boolean isExpired(String token) {
        return new Date().after(getExpiration(token));
    }

    public Long getId(String token) {
        return Long.parseLong(getBody(token).getSubject());
    }

    public Boolean isAdmin(String token) {
        System.out.println("role");
        System.out.println(getBody(token).get("role").toString());
        return getBody(token).get("role").toString().equalsIgnoreCase("ADMIN");
    }
}
