package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import ru.itis.dto.User;
import ru.itis.dto.UserDto;
import ru.itis.exceptions.ForbiddenException;
import ru.itis.redis.RedisUserService;

@Service
public class UserService {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    RedisUserService redisUserService;
    @Autowired
    AuthService authService;
    @Autowired
    JwtsHelper helper;


    @Value("${users.get.users.request.url}")
    private String getUsersUrl;
    @Value("${users.post.revoke.ban.request.url}")
    private String revokeBanUrl;
    @Value("${users.post.ban.request.url}")
    private String banUrl;


    public User[] getUsers(String token) {
        if (redisUserService.isBanned(token))
            throw new ForbiddenException();
        try {
            return restTemplate.exchange(getUsersUrl, HttpMethod.GET, authService.authHeader(token), User[].class).getBody();
        } catch (HttpClientErrorException e) {
            throw new ForbiddenException();
        }
    }

    public void ban(String token, String id) {
        banAction(true, token, id);
    }

    public void revokeBan(String token, String id) {
        banAction(false, token, id);
    }

    private void banAction(boolean ban, String token, String id) {
        if (!helper.isAdmin(token) || helper.isExpired(token) || redisUserService.isBanned(token))
            throw new ForbiddenException();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", token);

        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("id", id);

        try {
            restTemplate.exchange(ban ? banUrl : revokeBanUrl, HttpMethod.POST, new HttpEntity<>(body, headers), String.class);
        } catch (HttpClientErrorException e) {
            throw new ForbiddenException();
        }
    }
}
