package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import ru.itis.dto.LoginRequestDto;
import ru.itis.exceptions.BadRequestException;
import ru.itis.exceptions.ForbiddenException;
import ru.itis.redis.RedisUserService;

@Service
public class AuthService {
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    RedisUserService redisUserService;
    @Value("${users.login.request.url}")
    private String loginUserUrl;
    @Value("${users.logout.request.url}")
    private String logoutUserUrl;

    public String loginAndObtainToken(LoginRequestDto loginRequestDto) {
        System.out.println("obtain token");
        try {
            ResponseEntity<String> resp = restTemplate.exchange(loginUserUrl,
                    HttpMethod.POST,
                    new HttpEntity<Object>(loginRequestDto),
                    String.class);
            String token = resp.getBody();
            if (redisUserService.isBanned(token))
                throw new ForbiddenException();
            return token;
        } catch (HttpServerErrorException e) {
            throw new BadRequestException();
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.UNAUTHORIZED){
                throw new ForbiddenException();
            }
            e.printStackTrace();
            return null;
        }
    }

    public void logout(String token) {
        try {
            restTemplate.exchange(logoutUserUrl, HttpMethod.POST, authHeader(token), String.class);
        } catch (HttpClientErrorException e) {
            throw new ForbiddenException();
        }
    }

    public HttpEntity authHeader(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", token);

        return new HttpEntity<>(headers);
    }
}
