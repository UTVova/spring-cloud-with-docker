package ru.itis.amqp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.dto.RegistrationRequestDto;

@Component
public class RegistrationRequestsProducer {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private DirectExchange directExchange;
    @Autowired
    private ObjectMapper mapper;


    public void send(RegistrationRequestDto registrationRequestDto) {
        try {
            this.rabbitTemplate.convertAndSend(this.directExchange.getName(),
                    AmqpConfig.REQUESTS_FOR_REGISTRATION_ROUTING_KEY,
                    mapper.writeValueAsString(registrationRequestDto));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
