package ru.itis.amqp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import ru.itis.config.WebSocketConfig;
import ru.itis.dto.CatResponseDto;

import java.io.IOException;

@Component
public class CatsReceiver {

    @Autowired
    SimpMessagingTemplate messagingTemplate;

    @Autowired
    ObjectMapper mapper;

    @RabbitListener(queues = AmqpConfig.READY_CATS_QUEUE)
    public void returnCatToUser(String producedCat) {
        try {
            CatResponseDto catUrlResponse = mapper.readValue(producedCat, CatResponseDto.class);
            messagingTemplate.convertAndSendToUser(catUrlResponse.getUuid(),
                    WebSocketConfig.SUBSCRIBE_QUEUE + WebSocketConfig.SUBSCRIBE_CAT_RESPONSE, // /queue/cat_resp
                    catUrlResponse.getUrl());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
