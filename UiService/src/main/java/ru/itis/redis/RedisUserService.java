package ru.itis.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import ru.itis.services.JwtsHelper;

@Service
public class RedisUserService {

    @Autowired
    private RedisTemplate<String, Boolean> template;

    @Autowired
    JwtsHelper helper;

    public Boolean isBanned(String token) {
        return template.opsForValue().get(helper.getId(token).toString());
    }

}
