package ru.itis.dto;

import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class CatResponseDto {
    private String uuid;
    private String url;
}
