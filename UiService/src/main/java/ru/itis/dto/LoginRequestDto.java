package ru.itis.dto;

import lombok.Data;

@Data
public class LoginRequestDto {
    private String password;
    private String username;
}
