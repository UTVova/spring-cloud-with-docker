package ru.itis.dto;

import lombok.Data;

@Data
public class UserDto {
    private Long id;
    private String catUrl;
    private String username;
}
