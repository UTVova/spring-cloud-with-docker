package ru.itis.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
@Data
@Builder
public class User {
    private Long id;
    private String username;
    private String role;
    private String catUrl;
    private boolean banned;
}
