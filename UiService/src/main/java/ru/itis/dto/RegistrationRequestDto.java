package ru.itis.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RegistrationRequestDto {
    private String username;
    private String password;
    private String catUrl;
}
