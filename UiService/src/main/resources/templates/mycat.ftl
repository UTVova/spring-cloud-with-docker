<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html>
<head>
    <title>Index page</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<noscript><h2 style="color: #ff0000">Seems your browser doesn't support Javascript! Websocket relies on Javascript being
    enabled. Please enable
    Javascript and reload this page!</h2></noscript>
<div id="main-content" class="container">
    <div class="row">
        <div class="col-md-6">
            MY CAT
            <img id="catimg" src="" alt="" style="display: none; margin-top: 10px;">
        </div>
    </div>
</div>
</body>
<style>
    img {
        max-width: 500px;
        max-height: 500px;
    }
</style>
</html>