package ru.itis.services;

import io.jsonwebtoken.*;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.dto.LoginDto;
import ru.itis.models.Role;
import ru.itis.models.User;
import ru.itis.redis.RedisUserService;
import ru.itis.repositories.UserRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    RedisUserService redisUserService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder encoder;
    @Value("${jwt.secret:secret}")
    private String jwtSecret;

    public User save(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        user.setRole(Role.USER);
        return this.userRepository.save(user);
    }

    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    public User findOneByUsername(String username) {
        return userRepository.findOneByUsername(username).orElseThrow(()
                -> new IllegalArgumentException("User not found by login <" + username + ">"));
    }

    public Optional<User> userFromDto(LoginDto loginDto) {
        Optional<User> user = userRepository.findOneByUsername(loginDto.getUsername());
        return user.filter(u -> encoder.matches(loginDto.getPassword(), u.getPassword()));
    }

    public String obtainTokenByUser(User user) {
        System.out.println(user.getRole().toString());
        String token = Jwts.builder()
                .claim("role", user.getRole().toString())
                .claim("username", user.getUsername())
                .setSubject(user.getId().toString())
                .setExpiration(DateUtils.addSeconds(new Date(), 300))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
        redisUserService.addUser(user, 300);
        return token;
    }

    public void revokeTokenForUser(String token) {
        Claims body;
        try {
            body = Jwts.parser()
                    .setSigningKey(jwtSecret)
                    .parseClaimsJws(token)
                    .getBody();
            redisUserService.revokeTokenById(Long.parseLong(body.getSubject()));
        } catch (MalformedJwtException | SignatureException | ExpiredJwtException e) {
            e.printStackTrace();
        }
    }

    private void setBanStatusById(Long id, Boolean banned) {
        this.userRepository.setBanned(id, banned);
        this.redisUserService.setUserBanStatusById(id, banned);
    }

    public void banById(Long id) {
        setBanStatusById(id, Boolean.TRUE);
    }

    public void revokeBanById(Long id) {
        setBanStatusById(id, Boolean.FALSE);
    }
}
