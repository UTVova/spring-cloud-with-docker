package ru.itis.amqp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.models.User;
import ru.itis.services.UserService;

import java.io.IOException;

@Component
public class RegistartionRequestsReceiver {

    @Autowired
    ObjectMapper mapper;
    @Autowired
    UserService userService;

    @RabbitListener(queues = AmqpConfig.REQUESTS_FOR_REGISTRATION_QUEUE)
    public void registerUser(String registrationRequest) {
        try {
            System.out.println("registration request received");
            User user = mapper.readValue(registrationRequest, User.class);
            System.out.println(userService.save(user));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
