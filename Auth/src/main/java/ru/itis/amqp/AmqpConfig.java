package ru.itis.amqp;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AmqpConfig {
    static final String REQUESTS_FOR_REGISTRATION_QUEUE = "requests-for-registration-queue";
    static final String REQUESTS_FOR_REGISTRATION_ROUTING_KEY = "registration";

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("rabbit");
    }


    @Bean
    public Queue registrationRequestsQueue() {
        return new Queue(REQUESTS_FOR_REGISTRATION_QUEUE);
    }


    @Bean
    public Binding registrationRequestsQueueBinding() {
        return BindingBuilder.bind(registrationRequestsQueue())
                .to(directExchange())
                .with(REQUESTS_FOR_REGISTRATION_ROUTING_KEY);
    }
}

