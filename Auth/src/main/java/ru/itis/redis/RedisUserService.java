package ru.itis.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import ru.itis.models.User;

import java.util.concurrent.TimeUnit;

@Service
public class RedisUserService {

    @Autowired
    private RedisTemplate<String, Boolean> template;

    public void addUser(User user, Integer tokenExpirationInSeconds) {
        template.opsForValue().set(user.getId().toString(), user.isBanned(), tokenExpirationInSeconds, TimeUnit.SECONDS);
    }

    public void setUserBanStatusById(Long id, Boolean banned) {
        template.opsForValue().set(id.toString(), banned);
    }


    public void revokeTokenById(Long id) {
        template.delete(id.toString());
    }
}
