package ru.itis.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Builder
@Data
@Getter
public class LoginDto {
    private String username;
    private String password;
}
