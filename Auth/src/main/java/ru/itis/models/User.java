package ru.itis.models;

import lombok.*;

import javax.persistence.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
@Data
@Builder
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String username;
    private String password;
    @Enumerated(value = EnumType.STRING)
    @Column(name="role")
    private Role role = Role.USER;
    private String catUrl;
    private boolean banned;
//    @OneToMany(mappedBy = "user")
//    private List<Token> tokens;
}
