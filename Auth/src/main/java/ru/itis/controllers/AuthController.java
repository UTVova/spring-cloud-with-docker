package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.LoginDto;
import ru.itis.exceptions.NotAuthorizedRequest;
import ru.itis.models.User;
import ru.itis.redis.RedisUserService;
import ru.itis.services.UserService;

import javax.servlet.ServletRequest;
import java.util.Optional;

@RestController
public class AuthController {

    @Autowired
    UserService userService;

    @PostMapping("/auth/login")
    public ResponseEntity<String> login(@RequestBody LoginDto loginDto) {
        System.out.println("here");
        Optional<User> userOptional = userService.userFromDto(loginDto);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            if (user.isBanned())
                throw new NotAuthorizedRequest();
            return ResponseEntity.ok(userService.obtainTokenByUser(user));
        } else {
            throw new NotAuthorizedRequest();
        }
    }

    @PostMapping("/auth/logout")
    public ResponseEntity logout(@RequestHeader String authorization) {
        userService.revokeTokenForUser(authorization);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


}
