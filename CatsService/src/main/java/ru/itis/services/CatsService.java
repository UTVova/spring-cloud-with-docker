package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.itis.dto.CatDto;

@Service
public class CatsService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${cats.get.request.url}")
    private String catsGetRequestUrl;

    @Value("${cats.get.request.api-key}")
    private String apiKey;

    public String getCatUrl() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("x-api-key", apiKey);

        HttpEntity entity = new HttpEntity(headers);

        return restTemplate.exchange(
                catsGetRequestUrl, HttpMethod.GET, entity, CatDto[].class).getBody()[0].getUrl();
//        return "http://dazedimg.dazedgroup.netdna-cdn.com/830/azure/dazed-prod/1150/0/1150228.jpg";
    }
}

