package ru.itis.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

@Data
@Getter
@ToString
@Builder
public class CatRequestDto {
    private String uuid;
}
