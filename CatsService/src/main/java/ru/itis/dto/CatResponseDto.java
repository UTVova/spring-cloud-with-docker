package ru.itis.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CatResponseDto {
    private String uuid;
    private String url;
}
