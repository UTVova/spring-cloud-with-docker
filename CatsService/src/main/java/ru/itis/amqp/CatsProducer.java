package ru.itis.amqp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.dto.CatResponseDto;

@Component
public class CatsProducer {
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Autowired
    DirectExchange directExchange;
    @Autowired
    ObjectMapper mapper;

    public void send(CatResponseDto catResponseDto) {
        try {
            String catResponseJson = this.mapper.writeValueAsString(catResponseDto);
            System.out.println(catResponseJson);
            this.rabbitTemplate.convertAndSend(this.directExchange.getName(), Config.READY_CATS_ROUTING_KEY, catResponseJson);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
