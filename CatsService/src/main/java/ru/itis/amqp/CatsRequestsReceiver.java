package ru.itis.amqp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.dto.CatRequestDto;
import ru.itis.dto.CatResponseDto;
import ru.itis.services.CatsService;

import java.io.IOException;

@Component
public class CatsRequestsReceiver {

    @Autowired
    ObjectMapper mapper;
    @Autowired
    CatsProducer producer;
    @Autowired
    CatsService service;

    @RabbitListener(queues = Config.REQUESTS_FOR_CATS_QUEUE)
    public void findCatForUser(String catRequest) {
        try {
            CatRequestDto catRequestDto = mapper.readValue(catRequest, CatRequestDto.class);
            String url = service.getCatUrl();
            System.out.println(url);
            producer.send(CatResponseDto.builder()
                    .uuid(catRequestDto.getUuid())
                    .url(url)
                    .build());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
